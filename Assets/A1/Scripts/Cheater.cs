using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using StringActionPair = System.Collections.Generic.KeyValuePair<string, System.Action>;

namespace AnimalDisco {
public class Cheater : MonoBehaviour
{
    public NPCSpawner npcSpawner;
    public DiscoLightSpawner discoLightSpawner;
    public Sprite dogeSprite;
    public GameObject maskedAnimal;
    public Squidgame squidgame;
    public GameObject player;

    public DiscoBackground discoBackground;

    static readonly Dictionary<KeyCode, char> keyCodeToChar = new Dictionary<KeyCode, char>()
    {
        {KeyCode.A, 'a'},
        {KeyCode.B, 'b'},
        {KeyCode.C, 'c'},
        {KeyCode.D, 'd'},
        {KeyCode.E, 'e'},
        {KeyCode.F, 'f'},
        {KeyCode.G, 'g'},
        {KeyCode.H, 'h'},
        {KeyCode.I, 'i'},
        {KeyCode.J, 'j'},
        {KeyCode.K, 'k'},
        {KeyCode.L, 'l'},
        {KeyCode.M, 'm'},
        {KeyCode.N, 'n'},
        {KeyCode.O, 'o'},
        {KeyCode.P, 'p'},
        {KeyCode.Q, 'q'},
        {KeyCode.R, 'r'},
        {KeyCode.S, 's'},
        {KeyCode.T, 't'},
        {KeyCode.U, 'u'},
        {KeyCode.V, 'v'},
        {KeyCode.W, 'w'},
        {KeyCode.X, 'x'},
        {KeyCode.Y, 'y'},
        {KeyCode.Z, 'z'}
    };

    void Squidgame()
    {
        Debug.Log("Squidgame");
        squidgame.Running = true;
    }

    void Lockdown()
    {
        Debug.Log("Lockdown");
        Camera mainCam = Camera.main;
        GameObject maskedAnimalInstance = Instantiate(maskedAnimal, Vector3.zero, Quaternion.identity, transform);
        
        // Position above camera.
        Vector3 position = mainCam.ViewportToWorldPoint(new Vector3(0.5f, 1.0f, mainCam.nearClipPlane));
        position.y += 1.5f;
        position.x += (UnityEngine.Random.value - 0.5f) * 4.0f;
        maskedAnimalInstance.transform.position = position;
    }

    void Doge()
    {
        Debug.Log("Doge");
        foreach(Transform npcTransform in npcSpawner.transform)
        {
            SpriteRenderer npcSR = npcTransform.GetComponent<SpriteRenderer>();
            npcSR.sprite = dogeSprite;
            npcSR.flipY = true;
        }
    }

    void Dizzy() {
        Debug.Log("Dizzy");
        player.GetComponent<Player>().dizzy = true;
        npcSpawner.MakeDizzy();
    }

    void Spin() {
        Debug.Log("Spin");
        discoBackground.Spin = true;
    }

    StringActionPair[] cheatCodesToAction;

    int maxCheatCodeLength;

    List<char> input;

    void Start()
    {
        // The cheat codes are sorted by descending length.
        cheatCodesToAction = new StringActionPair[]
        {
            new StringActionPair("squidgame", Squidgame),
            new StringActionPair("lockdown", Lockdown),
            new StringActionPair("dizzy", Dizzy),
            new StringActionPair("doge", Doge),
            new StringActionPair("spin", Spin)
        };
        maxCheatCodeLength = cheatCodesToAction[0].Key.Length;
        input = new List<char>();
    }

    void Update()
    {
        foreach(KeyValuePair<KeyCode, char> keyCodeCharPair in keyCodeToChar)
        {
            if (Input.GetKeyDown(keyCodeCharPair.Key))
            {
                input.Add(keyCodeCharPair.Value);
                if (input.Count > maxCheatCodeLength)
                {
                    input.RemoveAt(0);
                }
                foreach(StringActionPair stringActionPair in cheatCodesToAction)
                {
                    if (input.Count >= stringActionPair.Key.Length)
                    {
                        bool isCheatCode = true;
                        for (int i = 0; i < stringActionPair.Key.Length; i++)
                        {
                            int k = stringActionPair.Key.Length - 1 - i;
                            int l = input.Count - 1 - i;
                            if (stringActionPair.Key[k] != input[l])
                            {
                                isCheatCode = false;
                                break;
                            }
                        }
                        if (isCheatCode)
                        {
                            stringActionPair.Value();
                            break;
                        }
                    }
                }
            }
        }
    }
}
}
