using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {
public class DiscoBackground : MonoBehaviour
{
    public bool Spin = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Spin) {
            transform.Rotate(new Vector3(0, 0, 45 * Time.deltaTime));
        }
    }
}
}
