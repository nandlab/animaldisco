using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {
public class SpawnUtils : MonoBehaviour
{
    public static Vector3 RandomWorldPosition()
    {
        Camera mainCam = Camera.main;
        return mainCam.ViewportToWorldPoint(
            new Vector3(Random.value, Random.value, mainCam.nearClipPlane)
        );
    }
}
}
