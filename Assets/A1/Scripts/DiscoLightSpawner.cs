using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {
public class DiscoLightSpawner : MonoBehaviour
{
    public SpriteRenderer discoLightSmall;
    public SpriteRenderer discoLightBase;
    public SpriteRenderer discoLightBig;
    int discoLightCount = 0;
    public Squidgame squidgame;

    SpriteRenderer RandomDiscoLight()
    {
        return Random.Range(0, 3) switch
        {
            0 => discoLightSmall,
            1 => discoLightBig,
            _ => discoLightBase
        };
    }

    void SpawnDiscoLight()
    {
        SpriteRenderer discoLight =
            Instantiate(RandomDiscoLight(), SpawnUtils.RandomWorldPosition(), Quaternion.identity, transform);
        discoLight.name = $"DiscoLight{discoLightCount:000}";
        discoLight.GetComponent<SpriteRenderer>().sortingOrder = -1;
        discoLight.GetComponent<DiscoLight>().squidgame = squidgame;
        discoLightCount++;
    }

    public void StartSquidgame()
    {
        foreach(Transform child in transform)
        {
            child.GetComponent<DiscoLight>().StartSquidGame();
        }
    }

    public void GreenLight()
    {
        foreach(Transform child in transform)
        {
            child.GetComponent<DiscoLight>().GreenLight();
        }
    }

    public void RedLight()
    {
        foreach(Transform child in transform)
        {
            child.GetComponent<DiscoLight>().RedLight();
        }        
    }

    void Start()
    {
        int randomNumber = Random.Range(4, 8);
        for (int i = 0; i < randomNumber; i++)
        {
            SpawnDiscoLight();
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            SpawnDiscoLight();
        }
    }
}
}
