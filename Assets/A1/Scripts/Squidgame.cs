using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {
public class Squidgame : MonoBehaviour
{
    public bool Running
    { get; set; }

    public bool RedLight
    { get; private set; }

    float duration = -1;

    void Start()
    {
    }

    void Update()
    {
        if (Running)
        {
            if (duration == -1)
            {
                duration = Random.Range(3.0f, 5.0f);
            }
            else
            {
                duration -= Time.deltaTime;
                if (duration <= 0.0f)
                {
                    RedLight = !RedLight;
                    if (RedLight)
                    {
                        duration = Random.Range(2.0f, 3.0f);
                    }
                    else
                    {
                        duration = Random.Range(3.0f, 6.0f);
                    }
                }
            }
        }
    }
}
}
