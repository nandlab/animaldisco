using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {

public class NPCSpawner : MonoBehaviour
{
    public GameObject npcPrefab;
    public Sprite[] npcSprites;
    public Squidgame squidgame;

    private List<GameObject> npcs;

    Sprite RandomSprite() {
        return npcSprites[Random.Range(0, npcSprites.Length)];
    }    

    public void MakeDizzy() {
        foreach (GameObject npc in npcs) {
            npc.GetComponent<NPC>().dizzy = true;
        }
    }

    void Start()
    {
        npcs = new List<GameObject>();
        int numberOfNPCs = Random.Range(5, 10);
        for (int i = 0; i < numberOfNPCs; i++) {
            GameObject npc = Instantiate(npcPrefab, transform);
            npc.name = $"NPC{i:000}";
            SpriteRenderer spriteRenderer = npc.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = RandomSprite();
            spriteRenderer.sortingOrder = 1;
            npc.GetComponent<NPC>().squidgame = squidgame;
            npc.transform.position = SpawnUtils.RandomWorldPosition();
            npcs.Add(npc);
        }
    }

    void Update()
    {
    }
}
}
