using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AnimalDisco {
public class Player : Dancer
{
    Transform parentTransform;
    SpriteRenderer playerSpriteRenderer;
    public Sprite[] playerSprites;
    bool stealth;
    int move;
    int spriteNum;
    public Squidgame squidgame;
    public bool dizzy = false;
 
    void SetPlayerAlpha(float alpha) {
        Color color = playerSpriteRenderer.color;
        color.a = alpha;
        playerSpriteRenderer.color = color;
    }

    static int GetMove() {
        if (Input.GetKey(KeyCode.Alpha1)) {
            return 1;
        }
        if (Input.GetKey(KeyCode.Alpha2)) {
            return 2;
        }
        if (Input.GetKey(KeyCode.Alpha3)) {
            return 3;
        }
        return 0;
    }

    bool Movement() {
        Vector3 moveVector = Vector3.zero;
        if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl)) {
            // Toggle stealth mode.
            stealth = !stealth;
            if (stealth) {
                SetPlayerAlpha(0.7f);
            }
            else {
                SetPlayerAlpha(1.0f);
            }                
        }
        if (Input.GetKey(KeyCode.W)) {
            moveVector.y += 1;
        }
        if (Input.GetKey(KeyCode.A)) {
            moveVector.x -= 1;
        }
        if (Input.GetKey(KeyCode.S)) {
            moveVector.y -= 1;
        }
        if (Input.GetKey(KeyCode.D)) {
            moveVector.x += 1;
        }
        if (moveVector.magnitude > 0.0f) {
            moveVector = 3.0f * moveVector.normalized * Time.deltaTime;
            if (stealth) {
                moveVector *= 0.6f;
            }
            else if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
                moveVector *= 1.4f;
            }
            parentTransform.localPosition += moveVector;
            return true;
        }
        return false;
    }

    void ChangePlayerSprite() {
        int choice = Random.Range(0, playerSprites.Length-1);
        if (choice >= spriteNum) {
            choice++;
        }
        spriteNum = choice;
        playerSpriteRenderer.sprite = playerSprites[spriteNum];
    }

    protected override void Start()
    {
        base.Start();
        parentTransform = transform.parent;
        playerSpriteRenderer = transform.GetComponent<SpriteRenderer>();
        spriteNum = 0;
        stealth = false;
        move = 0; // not dancing
    }

    protected override void Update()
    {
        base.Update();

        if (dizzy) {
            dizzyShake();
        }

        if (move == -1)
        {
            Color color = playerSpriteRenderer.color;
            float alpha = color.a;
            alpha -= Time.deltaTime / 2;
            if (alpha > 0.0f)
            {
                color.a = alpha;
                playerSpriteRenderer.color = color;
            }
            else
            {
                color.a = 0.0f;
                playerSpriteRenderer.color = color;
                Destroy(gameObject);
                int sceneIndex = SceneManager.GetActiveScene().buildIndex;
                SceneManager.LoadScene(sceneIndex);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                ChangePlayerSprite();
            }

            if (move == 0)
            {
                move = GetMove();
            }

            bool movement = false;

            if (move > 0)
            {
                SetPlayerAlpha(1.0f);
                movement = true;
            }

            switch (move)
            {
                case 0:
                    movement = Movement();
                    break;
                case 1:
                    if (DanceRotate())
                        move = 0;
                    break;
                case 2:
                    if (DanceHeadBobbing())
                        move = 0;
                    break;
                default: // move 3
                    if (DanceShake())
                        move = 0;
                    break;
            }
            
            if (squidgame.Running && squidgame.RedLight && movement)
            {
                move = -1;
            }
        }
    }
}
}
