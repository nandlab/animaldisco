using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {
public class NPC : Dancer
{
    int move; // -1: dying; 0: waiting; 1-3: dancing
    float duration;
    public Squidgame squidgame;

    public bool dizzy = false;

    void NextMove() {
        const int NUM_MOVES = 4;
        if (move > 0) {
            move = 0;
        }
        else {
            move = Random.Range(1, NUM_MOVES);
        }
        duration = move switch {
            0 => Random.Range(2.0f, 4.0f),
            1 => Random.Range(3, 8),
            2 => Random.Range(5, 12),
            _ => Random.Range(10, 20)
        };
    }

    protected override void Start()
    {
        base.Start();
        move = 0;
        duration = Random.Range(1.0f, 3.0f);
    }

    protected override void Update()
    {
        base.Update();

        if (dizzy) {
            dizzyShake();
        }

        switch (move) {
            case -1:
                SpriteRenderer renderer = GetComponent<SpriteRenderer>();
                Color color = renderer.color;
                float alpha = color.a;
                alpha -= Time.deltaTime / 2;
                if (alpha > 0.0f)
                {
                    color.a = alpha;
                    renderer.color = color;
                }
                else
                {
                    color.a = 0.0f;
                    Destroy(gameObject);
                }
                break;
            case 0:
                duration -= Time.deltaTime;
                break;
            case 1:
                if (DanceRotate())
                    duration--;
                break;
            case 2:
                if (DanceHeadBobbing())
                    duration--;
                break;
            default: // Dance 3
                if (DanceShake())
                    duration--;
                break;
        }

        if (squidgame.Running && squidgame.RedLight && move > 0)
        {
            move = -1;
        }
        else if (duration <= 0.0f) {
            NextMove();
        }
    }
}
}
