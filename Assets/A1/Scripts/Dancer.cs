using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {
public class Dancer : MonoBehaviour
{
    float playerRotation;
    float playerScale;
    bool headFwd; // Whether head is moved forward or back in dance 2.

    int shakeStep;
    float shakeOffset;
    float originalX;

    public int dizzyStep = 0;

    protected virtual void Start()
    {
        playerRotation = 0.0f;
        playerScale = 1.0f;
        headFwd = true;
        shakeStep = 0;
        shakeOffset = 0.0f;
        originalX = transform.localPosition.x;
    }
    
    protected virtual void Update()
    {
    }

    // Returns true if the dance move is completed.
    protected bool DanceRotate() {
        playerRotation += 720.0f * Time.deltaTime;
        if (playerRotation < 360.0f) {
            transform.localRotation = Quaternion.Euler(0, 0, playerRotation);
        }
        else {
            playerRotation = 0.0f;
            transform.localRotation = Quaternion.identity;
            return true;
        }
        return false;
    }

    // Returns true if the dance move is completed.
    protected bool DanceHeadBobbing() {
        const float bobbingSpeed = 2.0f;
        bool ready = false;
        if (headFwd) {
            playerScale += bobbingSpeed * Time.deltaTime;
            if (playerScale >= 1.2f) {
                playerScale = 1.2f;
                headFwd = false;
            }
        }
        else {
            playerScale -= bobbingSpeed * Time.deltaTime;
            if (playerScale <= 1.0f) {
                playerScale = 1.0f;
                headFwd = true;
                ready = true;
            }
        }
        transform.localScale = Vector3.one * playerScale;
        return ready;
    }

    // Returns true if the dance move is completed.
    protected bool DanceShake() {
        bool ready = false;
        const float shakeAmplitude = 0.3f;
        const float shakeSpeed = 6.0f;
        if (shakeStep == 0) {
            shakeOffset += shakeSpeed * Time.deltaTime;
            if (shakeOffset >= shakeAmplitude) {
                shakeOffset = shakeAmplitude;
                shakeStep++;
            }
        }
        else if (shakeStep == 1) {
            shakeOffset -= shakeSpeed * Time.deltaTime;
            if (shakeOffset <= 0.0f) {
                shakeOffset = 0.0f;
                shakeStep++;
                ready = true;
            }
        }
        else if (shakeStep == 2) {
            shakeOffset -= shakeSpeed * Time.deltaTime;
            if (shakeOffset <= -shakeAmplitude) {
                shakeOffset = -shakeAmplitude;
                shakeStep++;
            }
        }
        else {
            shakeOffset += shakeSpeed * Time.deltaTime;
            if (shakeOffset >= 0.0f) {
                shakeOffset = 0.0f;
                shakeStep = 0;
                ready = true;
            }
        }
        Vector3 position = transform.localPosition;
        position.x = originalX + shakeOffset;
        transform.localPosition = position;
        return ready;
    }

    protected void dizzyShake() {
        Vector3 scale = transform.localScale;
        const float dizzySpeed = 1.0f;
        float advance = dizzySpeed * Time.deltaTime;
        switch (dizzyStep) {
            case 0:
                scale.x += advance;
                scale.y -= advance;
                if (scale.x >= 1.5f) {
                    scale.x = 1.5f;
                    scale.y = 0.5f;
                    dizzyStep = 1;
                }
                break;
            case 1:
                scale.x -= advance;
                scale.y += advance;
                if (scale.y >= 1.5f) {
                    scale.x = 0.5f;
                    scale.y = 1.5f;
                    dizzyStep = 0;
                }
                break;
            default:
                break;
        }
        transform.localScale = scale;
    }
}
}
